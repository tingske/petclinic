package be.ae.training.model;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.safari.SafariDriver;

/**
 * Created by Wouter on 22/11/2015.
 */
public class BrowserFactory {

    public static WebDriver getWebDriver(BrowserFlavour type){
        switch (type){
            case chrome:
                return localChrome();
            case firefox:
                return localFirefox();
            case ie:
                return localIE();
            case ghost:
                return localGhost();
            case htmlunit:
                return localHtmlUnit();
            case safari:
                return localSafari();
            default:
                throw new IllegalArgumentException("Unsupported browser type");
        }
    }

    private static WebDriver localChrome(){
        if(System.getProperty("os.name").contains("Mac")){
            System.setProperty("webdriver.chrome.driver", "files/drivers/chromedriver");
        }else {
            System.setProperty("webdriver.chrome.driver", "files/drivers/chromedriver.exe");
        }
        return new ChromeDriver();
    }

    private static WebDriver localFirefox(){
        if(System.getProperty("os.name").contains("Mac")) {
            System.setProperty("webdriver.gecko.driver", "files/drivers/geckodriver");
        }else {
            System.setProperty("webdriver.gecko.driver", "files/drivers/geckodriver.exe");
        }
        return new FirefoxDriver();
    }

    private static WebDriver localIE(){
        System.setProperty("webdriver.ie.driver", "files/drivers/IEDriverServer.exe");
        return new InternetExplorerDriver();
    }

    private static WebDriver localGhost(){
        System.setProperty("phantomjs.binary.path", "files/headless/phantomjs.exe");
        return new PhantomJSDriver();
    }
    private static WebDriver localHtmlUnit(){

        HtmlUnitDriver wd = new HtmlUnitDriver(BrowserVersion.FIREFOX_52);
        wd.setJavascriptEnabled(true);
        wd.setAcceptSslCertificates(true);
        return wd;

    }

    private static WebDriver localSafari(){
        return new SafariDriver();
    }

}
