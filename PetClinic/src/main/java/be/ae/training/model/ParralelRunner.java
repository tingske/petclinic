//package be.ae.training.model;
//
//import be.ae.training.solutions.ajaxresults.AjaxResults_261;
//import be.ae.training.solutions.basicauth.BasicAuth_268;
//import be.ae.training.solutions.cookie.Cookie_2611;
//import be.ae.training.solutions.datadriven.DataDriven_2613;
//import be.ae.training.solutions.dropdown.DropDown_265;
//import be.ae.training.solutions.fileupload.FileUpload_269;
//import be.ae.training.solutions.frames.Frames_263;
//import be.ae.training.solutions.javascriptpopup.JavaScriptPopup;
//import be.ae.training.solutions.newtab.NewTab_2610;
//import be.ae.training.solutions.testretry.TestRetry_266;
//import be.ae.training.solutions.theinternet.TheInternet_262;
//import org.testng.TestNG;
//import org.testng.xml.XmlSuite;
//
//import java.util.Arrays;
//
///**
// * Created by Wouter on 22/11/2015.
// */
//public class ParralelRunner {
//
//    public static void main(String[] args) {
//        TestNG engine = new TestNG(true);
//
//        engine.setParallel(XmlSuite.ParallelMode.CLASSES);
//        engine.setThreadCount(10);
//
//        engine.setTestClasses(new Class[]{AjaxResults_261.class, BasicAuth_268.class, Cookie_2611.class, DropDown_265
//                .class, FileUpload_269.class, Frames_263.class, JavaScriptPopup.class, NewTab_2610.class,
//                TestRetry_266.class, TheInternet_262.class, DataDriven_2613.class});
//
//        //Uncomment to enable Screenshot on Failure Listener
//        engine.setListenerClasses(Arrays.asList(new Class[]{ScreenshotOnFailedListener.class}));
//        engine.run();
//
//
//    }
//}
