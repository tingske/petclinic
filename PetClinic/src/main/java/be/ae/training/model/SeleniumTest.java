package be.ae.training.model;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.io.File;
import java.io.IOException;

/**
 * Created by Wouter on 22/11/2015.
 */
public abstract class SeleniumTest {

    protected WebDriver browser;

    @Parameters("browserType")
    @BeforeMethod
    public void setUp(@Optional("notdeclared") String browserType){

        if(browserType.equalsIgnoreCase("htmlUnit")) {
            browser = BrowserFactory.getWebDriver(BrowserFlavour.htmlunit);
        }else if(browserType.equalsIgnoreCase("safari")) {
            browser = BrowserFactory.getWebDriver(BrowserFlavour.safari);
        }else if(browserType.equalsIgnoreCase("chrome")) {
            browser = BrowserFactory.getWebDriver(BrowserFlavour.chrome);
        }else if(browserType.equalsIgnoreCase("firefox")) {
            browser = BrowserFactory.getWebDriver(BrowserFlavour.firefox);
        }else{
            browser = BrowserFactory.getWebDriver(runInBrowser());
        }

        browser.manage().window().maximize();
        PageFactory.initElements(browser,this);
    }



    @AfterMethod
    public void tearDown(){
        browser.quit();
    }


    public abstract BrowserFlavour runInBrowser();

    public void waitForElement(WebElement element){
        new WebDriverWait(browser,3000).until(ExpectedConditions.visibilityOf(element));
    }

    public void waitInMS(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void takeScreenshot(){
        try {
            File scrFile = ((TakesScreenshot)browser).getScreenshotAs(OutputType.FILE);
            File out = new File("files/screenshots/");
            out.mkdirs();
            FileUtils.copyFile(scrFile, File.createTempFile("img_",".png",out));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
