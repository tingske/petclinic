package be.ae.training.model;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Created by Wouter on 22/11/2015.
 */
public class ScreenshotOnFailedListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult tr) {
        if(!(tr.getInstance() instanceof SeleniumTest))
            return;

        SeleniumTest test = (SeleniumTest) tr.getInstance();
        test.takeScreenshot();

    }
}
