package be.ae.training.model;

/**
 * Created by Wouter on 22/11/2015.
 */
public enum BrowserFlavour {
    firefox,chrome,ie,ghost,htmlunit,safari
}
