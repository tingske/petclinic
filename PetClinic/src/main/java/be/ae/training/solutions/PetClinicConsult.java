package be.ae.training.solutions;

//        https://spring-petclinic-community.herokuapp.com
//        https://stagemonitor-demo.isys-software.de/
//        Imagine you're a secretary of a Pet-Clinic.
//
//        A client is calling and she wants to make an appointment.
//        Her Dragonfly is in need of surgery.
//
//        * Create a new pet owner.
//        * Add a new pet to that owner.
//        ** The pet is a dragonfly.
//        ** You can name the pet however you like.
//
//        The client says: Aaah! Stupid me, it was my other Dragonfly which needs surgery!
//
//        * Update the pet name.
//        * Find a vet which is specialized in "surgery" and make a visit in the future.
//        Make sure to check for the name of the vet with the specialty "surgery".
//
//        At this point, the client her patience is running out and she doesn't have faith in your pet clinic anymore.
//        She says I'm leaving, according to GDPR you have to remove my data!
//        The lady on the phone asks you to delete the following data:
//
//        - The appointment
//        - The dragonflies detail
//        - The owners detail
//
//
//        Create this flow in Selenium and create a Testng.xml for it. Run this flow via Testng.xml on chrome & firefox.
//
//
//        You are free to call the pet whatever you like.
//        The owner details (name, is your name) address details, make something up!


import be.ae.training.model.BrowserFlavour;
import be.ae.training.model.SeleniumTest;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class PetClinicConsult extends SeleniumTest {

    String link = "https://spring-petclinic-community.herokuapp.com";
    String ownerId;
    String url;
    String petName;
    String vet;
    String lastName,firstName;

    //homepage
    @FindBy(css = "a[href=\"/owners/find\"]")
    WebElement findOwnersTab;

    @FindBy(css = "a[href=\"/vets.html\"]")
    WebElement findVetTab;

    //find owners page
    @FindBy(css = "a[href=\"/owners/new\"]")
    WebElement addOwnerButton;

    @FindBy(id="lastName")
    WebElement findOwnerTextBox;

    @FindBy(css = "button[type=\"submit\"]")
    WebElement findOwnerButton;

    //add owner page
    @FindBy(id="firstName")
    WebElement firstNameTextBox;

    @FindBy(id="lastName")
    WebElement lastNameTextBox;

    @FindBy(id="address")
    WebElement addressTextBox;

    @FindBy(id="city")
    WebElement cityTextBox;

    @FindBy(id="telephone")
    WebElement telephoneTextBox;

    @FindBy(css = "button[type=\"submit\"]")
    WebElement submitOwnerButton;

    //Owner information
    @FindBy(partialLinkText = "Add New Pet")
    WebElement addPetButton;

    //new Pet page
    @FindBy(id="name")
    WebElement nameTextBox;

    @FindBy(id="birthDate")
    WebElement birthDateTextBox;

    @FindBy(css = "button[type=\"submit\"]")
    WebElement submitPetButton;

    //update Pet page
    @FindBy(css = "button[type=\"submit\"]")
    WebElement updatePetButton;

    //make new visit page
    @FindBy(id="date")
    WebElement visitDateTextBox;

    @FindBy(id="description")
    WebElement descriptionTextBox;

    @FindBy(css = "button[type=\"submit\"]")
    WebElement submitvisitButton;



    @Test
    public void test1(){
        //create
        browser.get(link);
        findOwnersTab.click();
        addOwnerButton.click();
        firstName = "Ting";
        lastName = "Lee";
        fillOwnerDetails();
        url = browser.getCurrentUrl();
        ownerId = url.replace(link + "/owners/","");
        browser.findElement(By.xpath("//a[@href=\""+ownerId+"/pets/new\"]")).click();
        nameTextBox.sendKeys("test");
        birthDateTextBox.sendKeys("2018-01-23");
        Select drpPetType = new Select(browser.findElement(By.id("type")));
        drpPetType.selectByVisibleText("cat");
        submitPetButton.click();

        //update
        findOwner();
        waitForElement(addPetButton);
        browser.findElement(By.xpath("//a[contains(@href, '"+ownerId+"/pets/') and contains(@href, '/edit')][1]")).click();
        petName = browser.findElement(By.xpath("//input[@id='name']")).getAttribute("value");
        nameTextBox.clear();
        nameTextBox.sendKeys(petName + "01");
        updatePetButton.click();

        //make visit
        findVetTab.click();
        vet = browser.findElement(By.xpath("//span[1][contains(text(), \"surgery\")]/parent::*/parent::*/td[1]")).getText();
        findOwner();
        browser.findElement(By.xpath("//a[contains(@href, '"+ownerId+"/pets/') and contains(@href, '/visits/new')][1]")).click();
        visitDateTextBox.clear();
        visitDateTextBox.sendKeys("2019-12-12");
        descriptionTextBox.sendKeys(petName + "01 surgery from vet "+ vet);
        submitvisitButton.click();

        //'delete' owner
        findOwner();
        browser.findElement(By.xpath("//a[@href=\""+ownerId+"/edit\"]")).click();
        firstNameTextBox.clear();
        firstNameTextBox.sendKeys("Delete " + firstName);
        lastNameTextBox.clear();
        lastNameTextBox.sendKeys("Delete " + lastName);
        submitOwnerButton.click();

    }

    private void findOwner() {
        findOwnersTab.click();
        findOwnerTextBox.sendKeys(lastName);
        findOwnerButton.click();
        try{
            browser.findElement(By.id("vets")).isDisplayed();
            System.out.println("duplicate owners found: picking the one that we've last created");
            browser.findElement(By.xpath("//a[@href=\"/owners/"+ownerId+"\"]")).click();
        } catch (NoSuchElementException e){
            System.out.println("no duplicate owners found");
        }
    }

    private void fillOwnerDetails(){
        firstNameTextBox.sendKeys(firstName);
        lastNameTextBox.sendKeys(lastName);
        addressTextBox.sendKeys("test");
        cityTextBox.sendKeys("test");
        telephoneTextBox.sendKeys("0000");
        submitOwnerButton.click();
    }

    @Override
    public BrowserFlavour runInBrowser() {
        return BrowserFlavour.firefox;
    }
}
